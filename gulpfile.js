//SAS Compiler e PHP-Sync para subpasta no root
// npm install -g gulp
// npm install -g browser-sync
// npm install --save-dev gulp
// npm install --save-dev browser-sync
// npm install --save-dev gulp-connect-php
// npm i browser-sync --save


var gulp = require('gulp');
var browsersync = require('browser-sync').create();
var php = require('gulp-connect-php');

//servidor para olhar os Html /scss
gulp.task('server', gulp.series( function() {
    browsersync.init({
        proxy:"localhost:3000/public",
        baseDir: "./public",
        open:true,
        notify:false

    });
    
    php.server({base:'./public', port:3000, keepalive:true});
    gulp.watch("./public/**/*.*").on('change',gulp.parallel( browsersync.reload));

}));

// =====================================================
gulp.task('PROJETOS', gulp.series( ['server']));